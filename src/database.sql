CREATE TABLE IF NOT EXISTS pings (
    pingid INT PRIMARY KEY,

    destination TEXT NOT NULL, -- ip address

    utc FLOAT NOT NULL,
    num_replys INT NOT NULL DEFAULT(0),
    first_reply_time FLOAT,
    last_reply_time FLOAT
);

CREATE INDEX IF NOT EXISTS pings_utc ON pings(utc);

CREATE TABLE IF NOT EXISTS destinations (
    destination TEXT NOT NULL PRIMARY KEY
);
