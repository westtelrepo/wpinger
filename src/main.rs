#![forbid(unsafe_code)]

mod com_westtel_wpinger;

use chrono::prelude::*;
use chrono_english::{parse_date_string, Dialect};
use com_westtel_wpinger::{
    Call_AddDestinations, Call_ClearData, Call_ClearDataForDests, Call_DeleteDestinations,
    Call_ExportAll, Call_ListDestinations, Call_Stat, Call_StatTime, ExportRow, StatResultRow,
    StatTimeRow, VarlinkClientInterface, VarlinkInterface,
};
use crossbeam::scope;
use duct::cmd;
use nix::unistd::Uid;
use pnet::packet::icmp::echo_reply::EchoReplyPacket;
use pnet::packet::icmp::echo_request::{EchoRequestPacket, MutableEchoRequestPacket};
use pnet::packet::icmp::{checksum, IcmpCode, IcmpPacket, IcmpTypes};
use pnet::packet::ip::IpNextHeaderProtocols::Icmp;
use pnet::packet::Packet;
use pnet::transport::{
    icmp_packet_iter, transport_channel, TransportChannelType::Layer4, TransportProtocol::Ipv4,
};
use rusqlite::{
    functions::{Aggregate, Context, FunctionFlags},
    params, Connection, OpenFlags, Transaction, TransactionBehavior,
};
use serde::{Deserialize, Serialize};
use std::io::{ErrorKind, Write};
use std::net::{IpAddr, Ipv4Addr};
use std::os::unix::fs::PermissionsExt;
use std::sync::{mpsc, RwLock};
use std::sync::{Arc, Mutex};
use std::thread::sleep;
use std::time::{Duration, Instant, SystemTime, UNIX_EPOCH};
use std::{cmp::Ordering, fs};
use std::{collections::HashMap, fs::Permissions};
use structopt::StructOpt;

struct PercentileAgg {}

struct PercentileAggContext {
    pub vec: Vec<f32>,
    pub perc: f32,
}

impl Aggregate<PercentileAggContext, Option<f32>> for PercentileAgg {
    fn init(&self, _: &mut Context<'_>) -> rusqlite::Result<PercentileAggContext> {
        Ok(PercentileAggContext {
            vec: Vec::new(),
            perc: 0.0,
        })
    }

    fn step(&self, ctx: &mut Context<'_>, agg: &mut PercentileAggContext) -> rusqlite::Result<()> {
        let val: Option<f32> = ctx.get(0)?;
        if let Some(val) = val {
            agg.vec.push(val);
        }
        agg.perc = ctx.get(1)?;
        Ok(())
    }

    fn finalize(
        &self,
        _: &mut Context<'_>,
        agg: Option<PercentileAggContext>,
    ) -> rusqlite::Result<Option<f32>> {
        if let Some(mut agg) = agg {
            if agg.vec.is_empty() {
                return Ok(None);
            }

            agg.vec
                .sort_unstable_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal));

            let index =
                ((agg.vec.len() as f32) * agg.perc).clamp(0.0, agg.vec.len() as f32 - 1.0) as usize;

            Ok(Some(agg.vec[index.clamp(0, agg.vec.len() - 1)]))
        } else {
            Ok(None)
        }
    }
}

#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(subcommand)]
    cmd: OptCommand,
}

#[derive(StructOpt, Debug)]
enum OptCommand {
    /// run the service, used in the service file
    RunServer,
    /// view basic stats
    Stat { from_time: String, to_time: String },
    /// view basic stats over time
    StatTime {
        /// destination IP address
        dest: Ipv4Addr,
        /// first timestamp
        from_time: String,
        /// last timestamp
        to_time: String,
        #[structopt(default_value = "20")]
        /// number of intervals to create
        num_intervals: u32,
    },
    /// manage ping destinations
    Dest {
        #[structopt(subcommand)]
        cmd: DestCommand,
    },
    /// manage installation
    #[structopt(name = "self")]
    SelfCmd {
        #[structopt(subcommand)]
        cmd: SelfCmd,
    },
    /// Export data, do in an empty directory!
    ///
    /// Exports a CSV file for every destination in the database
    Export,
    /// Clear database data. WARNING: does not prompt!
    Clear {
        #[structopt(subcommand)]
        cmd: ClearCmd,
    },
}

#[derive(StructOpt, Debug)]
enum DestCommand {
    /// add ping destinations
    Add { dests: Vec<Ipv4Addr> },
    /// list existing ping destinations
    List,
    /// delete ping destinations
    Del { dests: Vec<Ipv4Addr> },
}

#[derive(StructOpt, Debug)]
enum SelfCmd {
    /// install and start service
    Install {
        /// don't try to run systemctl commands that won't work if systemd isn't booted
        #[structopt(long)]
        systemd_not_booted: bool,
        /// add `ExecStartPre=/bin/chronyc waitsync` to the service file
        #[structopt(long)]
        wait_for_chronyc_waitsync: bool,
        /// use newer security and convenience options for service (Debian Buster or above)
        #[structopt(long)]
        systemd_at_least_241: bool,
        /// install the systemd service files in /usr instead of /etc
        #[structopt(long)]
        systemd_in_usr: bool,
    },
    /// stop and remove service (warning: no prompt!)
    Uninstall,
}

#[derive(StructOpt, Debug)]
enum ClearCmd {
    /// clear ALL ping data without prompt!
    All,
    /// clear data for specific destinations
    Dests {
        /// destinations to clear data for
        dests: Vec<Ipv4Addr>,
    },
}

enum DbMessage {
    Initial(i64, SystemTime, Ipv4Addr),
    Reply(i64, SystemTime),
    Clean,
}

#[derive(Serialize, Deserialize, Debug)]
struct PingData {
    pingid: i64,
}

const DATABASE_SQL: &str = include_str!("database.sql");
const DATABASE_PATH: &str = "/var/lib/wpinger/data.db";

fn get_varlink_client() -> anyhow::Result<com_westtel_wpinger::VarlinkClient> {
    let connection = varlink::Connection::with_address("unix:/run/com.westtel.wpinger")?;
    Ok(com_westtel_wpinger::VarlinkClient::new(connection))
}

fn main() {
    let opt = Opt::from_args();

    match opt.cmd {
        OptCommand::RunServer => run_server(),
        OptCommand::Stat { from_time, to_time } => {
            let now = Local::now();
            let from = parse_date_string(&from_time, now, Dialect::Us).unwrap();
            let to = parse_date_string(&to_time, now, Dialect::Us).unwrap();

            let mut varlink = get_varlink_client().unwrap();
            println!("stat from {} to {}", from.to_rfc2822(), to.to_rfc2822());

            let result = varlink
                .stat(from.timestamp(), to.timestamp())
                .call()
                .unwrap()
                .result;
            for row in result {
                println!(
                    "{}: {:.2?}% of {} pings, max {:.2?}ms, 99%: {:.2?}ms, 95%: {:.2?}ms, avg {:.2?}ms, 1%: {:.2?}ms, min {:.2?}ms",
                    row.destination,
                    100.0 * row.perc,
                    row.num_pings,
                    row.max_ms.unwrap_or(std::f64::NAN),
                    row.perc_99.unwrap_or(std::f64::NAN),
                    row.perc_95.unwrap_or(std::f64::NAN),
                    row.avg_ms.unwrap_or(std::f64::NAN),
                    row.perc_1.unwrap_or(std::f64::NAN),
                    row.min_ms.unwrap_or(std::f64::NAN),
                );
            }
        }
        OptCommand::StatTime {
            dest,
            from_time,
            to_time,
            num_intervals,
        } => {
            let now = Local::now();
            let from = parse_date_string(&from_time, now, Dialect::Us).unwrap();
            let to = parse_date_string(&to_time, now, Dialect::Us).unwrap();

            let mut varlink = get_varlink_client().unwrap();
            println!(
                "time stat from {} to {} for {}",
                from.to_rfc2822(),
                to.to_rfc2822(),
                dest
            );

            let result = varlink
                .stat_time(
                    dest.to_string(),
                    from.timestamp(),
                    to.timestamp(),
                    num_intervals as i64,
                )
                .call()
                .unwrap()
                .result;

            for row in result {
                println!(
                    "{} to {}: {:.2?}% of {} pings, max {:.2?}ms, 99%: {:.2?}ms, 95%: {:.2?}ms, avg {:.2?}ms, 1%: {:.2?}ms, min {:.2?}ms",
                    Local.timestamp(row.from_utc, 0).to_rfc3339(),
                    Local.timestamp(row.to_utc, 0).to_rfc3339(),
                    100.0 * row.perc,
                    row.num_pings,
                    row.max_ms.unwrap_or(std::f64::NAN),
                    row.perc_99.unwrap_or(std::f64::NAN),
                    row.perc_95.unwrap_or(std::f64::NAN),
                    row.avg_ms.unwrap_or(std::f64::NAN),
                    row.perc_1.unwrap_or(std::f64::NAN),
                    row.min_ms.unwrap_or(std::f64::NAN)
                );
            }
        }
        OptCommand::Dest { cmd } => match cmd {
            DestCommand::Add { dests } => {
                let mut v = get_varlink_client().unwrap();
                v.add_destinations(dests.into_iter().map(|e| e.to_string()).collect())
                    .call()
                    .unwrap();
            }
            DestCommand::List => {
                let mut v = get_varlink_client().unwrap();
                let dest = v.list_destinations().call().unwrap().destinations;
                println!(
                    "{:?}",
                    dest.into_iter()
                        .map(|e| e.parse::<Ipv4Addr>().unwrap())
                        .collect::<Vec<_>>()
                );
            }
            DestCommand::Del { dests } => {
                let mut v = get_varlink_client().unwrap();
                v.delete_destinations(dests.into_iter().map(|e| e.to_string()).collect())
                    .call()
                    .unwrap();
            }
        },
        OptCommand::SelfCmd { cmd } => {
            match cmd {
                SelfCmd::Install {
                    systemd_not_booted,
                    wait_for_chronyc_waitsync,
                    systemd_at_least_241,
                    systemd_in_usr,
                } => {
                    install(
                        systemd_not_booted,
                        wait_for_chronyc_waitsync,
                        systemd_at_least_241,
                        systemd_in_usr,
                    );
                }
                SelfCmd::Uninstall => {
                    if !Uid::current().is_root() {
                        eprintln!("This command must be ran as root, did you forget `sudo`");
                        return;
                    }

                    // remove service
                    println!("disabling and stopping wpinger.service...");
                    cmd!("systemctl", "disable", "wpinger.service")
                        .unchecked()
                        .run()
                        .unwrap();
                    cmd!("systemctl", "stop", "wpinger.service")
                        .unchecked()
                        .run()
                        .unwrap();

                    println!("removing wpinger.service and wpinger.socket...");
                    match fs::remove_file("/etc/systemd/system/wpinger.service") {
                        Err(ref e) if e.kind() == ErrorKind::NotFound => {}
                        other => other.unwrap(),
                    };
                    match fs::remove_file("/usr/lib/systemd/system/wpinger.service") {
                        Err(ref e) if e.kind() == ErrorKind::NotFound => {}
                        other => other.unwrap(),
                    };
                    match fs::remove_file("/etc/systemd/system/wpinger.socket") {
                        Err(ref e) if e.kind() == ErrorKind::NotFound => {}
                        other => other.unwrap(),
                    };
                    match fs::remove_file("/usr/lib/systemd/system/wpinger.socket") {
                        Err(ref e) if e.kind() == ErrorKind::NotFound => {}
                        other => other.unwrap(),
                    };
                    cmd!("systemctl", "daemon-reload").run().unwrap();

                    // remove all data!
                    println!("deleting data directory /var/lib/wpinger...");
                    match fs::remove_dir_all("/var/lib/wpinger") {
                        Err(ref e) if e.kind() == ErrorKind::NotFound => {}
                        other => other.unwrap(),
                    };
                    match fs::remove_dir_all("/var/lib/private/wpinger") {
                        Err(ref e) if e.kind() == ErrorKind::NotFound => {}
                        other => other.unwrap(),
                    };

                    // remove self as last step
                    println!("deleting self /usr/local/bin/wpinger...");
                    match fs::remove_file("/usr/local/bin/wpinger") {
                        Err(ref e) if e.kind() == ErrorKind::NotFound => {}
                        other => other.unwrap(),
                    }

                    println!("Uninstalled!");
                }
            }
        }
        OptCommand::Export => {
            let mut varlink = get_varlink_client().unwrap();

            let mut files = HashMap::<String, std::fs::File>::new();

            for reply in varlink.export_all().more().unwrap() {
                let reply = reply.unwrap();
                for line in reply.data {
                    if !files.contains_key(&line.d) {
                        files.insert(
                            line.d.clone(),
                            std::fs::File::create(line.d.clone() + ".csv").unwrap(),
                        );
                    }

                    let mut file = files.get(&line.d).unwrap();
                    file.write_all(format!("{},{}\n", line.u, line.m).as_bytes())
                        .unwrap();
                }
            }
        }
        OptCommand::Clear { cmd } => match cmd {
            ClearCmd::All => {
                let mut varlink = get_varlink_client().unwrap();
                varlink.clear_data().call().unwrap();
            }
            ClearCmd::Dests { dests } => {
                let mut varlink = get_varlink_client().unwrap();
                varlink
                    .clear_data_for_dests(dests.iter().map(|e| e.to_string()).collect())
                    .call()
                    .unwrap();
            }
        },
    };
}

pub struct VarlinkServer {
    pub db_rw: Arc<Mutex<Connection>>,
    pub db_ro: Arc<Mutex<Connection>>,
    pub orig_destinations: Arc<RwLock<Vec<Ipv4Addr>>>,
}

fn get_destinations_tx(tx: &Transaction) -> anyhow::Result<Vec<Ipv4Addr>> {
    let mut stmt = tx.prepare("SELECT destination FROM destinations ORDER BY 1")?;
    let mut rows = stmt.query([])?;

    let mut ret = Vec::new();
    while let Some(ip) = rows.next()? {
        ret.push(ip.get::<_, String>(0)?.parse::<Ipv4Addr>()?);
    }

    Ok(ret)
}

fn add_destinations(
    db_rw: Arc<Mutex<Connection>>,
    destinations: Vec<String>,
    orig_destinations: Arc<RwLock<Vec<Ipv4Addr>>>,
) -> anyhow::Result<()> {
    let mut db = db_rw.lock().unwrap();

    let tx = db.transaction()?;

    for dest in destinations {
        let ip: Ipv4Addr = dest.parse()?;
        tx.execute(
            "INSERT OR IGNORE INTO destinations(destination) VALUES(?)",
            params!(ip.to_string()),
        )?;
    }

    let new_destinations = get_destinations_tx(&tx)?;

    tx.commit()?;
    *orig_destinations.write().unwrap() = new_destinations;
    Ok(())
}

fn delete_destinations(
    db_rw: Arc<Mutex<Connection>>,
    destinations: Vec<String>,
    orig_destinations: Arc<RwLock<Vec<Ipv4Addr>>>,
) -> anyhow::Result<()> {
    let mut db = db_rw.lock().unwrap();

    let tx = db.transaction()?;

    for dest in destinations {
        let ip: Ipv4Addr = dest.parse()?;
        tx.execute(
            "DELETE FROM destinations WHERE destination = ?",
            params!(ip.to_string()),
        )?;
    }

    let new_destinations = get_destinations_tx(&tx)?;

    tx.commit()?;
    *orig_destinations.write().unwrap() = new_destinations;
    Ok(())
}

fn stat_db(
    db_ro: Arc<Mutex<Connection>>,
    from: i64,
    to: i64,
) -> anyhow::Result<Vec<StatResultRow>> {
    let db = db_ro.lock().unwrap();

    let mut stmt = db.prepare("SELECT destination, CAST(COUNT(first_reply_time) AS REAL)/COUNT(*), COUNT(*), 1000 * MAX(first_reply_time - utc), 1000 * AVG(first_reply_time - utc), 1000 * MIN(first_reply_time - utc), 1000 * wt_percentile(first_reply_time - utc, 0.01), 1000 * wt_percentile(first_reply_time - utc, 0.99), 1000 * wt_percentile(first_reply_time - utc, 0.95) FROM pings WHERE utc >= ? AND utc < ? GROUP BY 1 ORDER BY 1")?;

    let mut rows = stmt.query(params!(from, to))?;

    let mut ret = Vec::new();

    while let Some(row) = rows.next()? {
        ret.push(StatResultRow {
            destination: row.get::<_, String>(0)?,
            perc: row.get::<_, f64>(1)?,
            num_pings: row.get::<_, i64>(2)?,
            max_ms: row.get::<_, Option<f64>>(3)?,
            avg_ms: row.get::<_, Option<f64>>(4)?,
            min_ms: row.get::<_, Option<f64>>(5)?,
            perc_1: row.get::<_, Option<f64>>(6)?,
            perc_99: row.get::<_, Option<f64>>(7)?,
            perc_95: row.get::<_, Option<f64>>(8)?,
        });
    }

    Ok(ret)
}

fn stat_time_db(
    db_ro: Arc<Mutex<Connection>>,
    dest: String,
    from: i64,
    to: i64,
    num_intervals: i64,
) -> anyhow::Result<Vec<StatTimeRow>> {
    anyhow::ensure!(num_intervals >= 1, "num_intervals must be positive");

    let interval = (to - from) / num_intervals;

    let dest: Ipv4Addr = dest.parse()?;
    let dest = dest.to_string();

    let mut db = db_ro.lock().unwrap();
    let tx = db.transaction()?;

    let mut ret = Vec::new();

    {
        let mut stmt = tx.prepare("SELECT CAST(COUNT(first_reply_time) AS REAL)/COUNT(*), COUNT(*), 1000 * MAX(first_reply_time - utc), 1000 * AVG(first_reply_time - utc), 1000 * MIN(first_reply_time - utc), 1000 * wt_percentile(first_reply_time - utc, 0.01), 1000 * wt_percentile(first_reply_time - utc, 0.95), 1000 * wt_percentile(first_reply_time - utc, 0.99) FROM pings WHERE utc >= ? AND utc < ? AND destination = ?").unwrap();

        for i in 0..num_intervals {
            let begin_t = from + i * interval;
            let end_t = from + (i + 1) * interval;
            stmt.query_row(params!(begin_t, end_t, dest), |row| {
                ret.push(StatTimeRow {
                    from_utc: begin_t,
                    to_utc: end_t,
                    perc: row.get::<_, Option<f64>>(0)?.unwrap_or(0.0),
                    num_pings: row.get::<_, i64>(1)?,
                    max_ms: row.get::<_, Option<f64>>(2)?,
                    avg_ms: row.get::<_, Option<f64>>(3)?,
                    min_ms: row.get::<_, Option<f64>>(4)?,
                    perc_1: row.get::<_, Option<f64>>(5)?,
                    perc_95: row.get::<_, Option<f64>>(6)?,
                    perc_99: row.get::<_, Option<f64>>(7)?,
                });

                Ok(())
            })?;
        }
    }

    tx.commit()?;

    Ok(ret)
}

fn export_all_helper(
    db_ro: Arc<Mutex<Connection>>,
    call: &mut dyn Call_ExportAll,
) -> anyhow::Result<()> {
    if !call.wants_more() {
        return call
            .reply_invalid_parameter("more required".to_string())
            .map_err(|e| e.into());
    }

    call.set_continues(true);
    let mut db_ro = db_ro.lock().unwrap();
    let tx = db_ro.transaction()?;

    let mut stmt = tx.prepare("SELECT destination, utc, 1000 * (first_reply_time - utc) FROM pings WHERE first_reply_time IS NOT NULL")?;

    let mut rows = stmt.query([])?;

    while let Some(row) = rows.next()? {
        call.reply(vec![ExportRow {
            d: row.get::<_, String>(0)?,
            u: row.get::<_, f64>(1)?,
            m: row.get::<_, f64>(2)?,
        }])?;
    }

    Ok(())
}

fn clear_data(db: Arc<Mutex<Connection>>) -> anyhow::Result<()> {
    let db = db.lock().unwrap();
    db.execute("DELETE FROM pings", [])?;
    db.execute("VACUUM", [])?;
    {
        let mut stmt = db.prepare("PRAGMA wal_checkpoint(TRUNCATE)")?;
        let mut rows = stmt.query([])?;
        while rows.next()?.is_some() {
            // ignore
        }
    }
    Ok(())
}

fn clear_data_for_dests(db: Arc<Mutex<Connection>>, dests: &[String]) -> anyhow::Result<()> {
    let mut db = db.lock().unwrap();
    let tx = db.transaction()?;

    for dest in dests {
        tx.execute("DELETE FROM pings WHERE destination = $1", [dest])?;
    }

    tx.commit()?;

    Ok(())
}

impl VarlinkInterface for VarlinkServer {
    fn add_destinations(
        &self,
        call: &mut dyn Call_AddDestinations,
        r#destinations: Vec<String>,
    ) -> varlink::Result<()> {
        match add_destinations(
            self.db_rw.clone(),
            r#destinations,
            self.orig_destinations.clone(),
        ) {
            Ok(()) => call.reply(),
            Err(e) => call.reply_invalid_parameter(e.to_string()),
        }
    }

    fn list_destinations(&self, call: &mut dyn Call_ListDestinations) -> varlink::Result<()> {
        let destinations = self.orig_destinations.read().unwrap();
        let mut result = Vec::new();
        for i in destinations.clone().into_iter() {
            result.push(i.to_string());
        }
        call.reply(result)
    }

    fn delete_destinations(
        &self,
        call: &mut dyn Call_DeleteDestinations,
        r#destinations: Vec<String>,
    ) -> varlink::Result<()> {
        match delete_destinations(
            self.db_rw.clone(),
            r#destinations,
            self.orig_destinations.clone(),
        ) {
            Ok(()) => call.reply(),
            Err(e) => call.reply_invalid_parameter(e.to_string()),
        }
    }

    fn stat(&self, call: &mut dyn Call_Stat, r#from: i64, r#to: i64) -> varlink::Result<()> {
        match stat_db(self.db_ro.clone(), r#from, r#to) {
            Ok(ret) => call.reply(ret),
            Err(e) => call.reply_invalid_parameter(e.to_string()),
        }
    }

    fn stat_time(
        &self,
        call: &mut dyn Call_StatTime,
        r#dest: String,
        r#from: i64,
        r#to: i64,
        r#num_intervals: i64,
    ) -> varlink::Result<()> {
        match stat_time_db(self.db_ro.clone(), r#dest, r#from, r#to, r#num_intervals) {
            Ok(result) => call.reply(result),
            Err(e) => call.reply_invalid_parameter(e.to_string()),
        }
    }

    fn export_all(&self, call: &mut dyn Call_ExportAll) -> varlink::Result<()> {
        let result = export_all_helper(self.db_ro.clone(), call);
        call.set_continues(false);
        match result {
            Err(e) => call.reply_invalid_parameter(e.to_string()),
            Ok(()) => call.reply(Vec::new()),
        }
    }

    fn clear_data(&self, call: &mut dyn Call_ClearData) -> varlink::Result<()> {
        match clear_data(self.db_rw.clone()) {
            Err(e) => call.reply_invalid_parameter(e.to_string()),
            Ok(()) => call.reply(),
        }
    }

    fn clear_data_for_dests(
        &self,
        call: &mut dyn Call_ClearDataForDests,
        r#dests: Vec<String>,
    ) -> varlink::Result<()> {
        match clear_data_for_dests(self.db_rw.clone(), &r#dests) {
            Err(e) => call.reply_invalid_parameter(e.to_string()),
            Ok(()) => call.reply(),
        }
    }
}

fn run_server() {
    {
        // kill everything if any thread panics
        let orig_hook = std::panic::take_hook();
        std::panic::set_hook(Box::new(move |info| {
            orig_hook(info);
            std::process::exit(1);
        }));
    }

    let db = {
        let mut ret = Connection::open(DATABASE_PATH).unwrap();
        ret.pragma_update(None, "journal_mode", &"wal").unwrap();
        ret.pragma_update(None, "synchronous", &"normal").unwrap();
        let tx = ret
            .transaction_with_behavior(TransactionBehavior::Exclusive)
            .unwrap();
        tx.execute_batch(DATABASE_SQL).unwrap();
        tx.commit().unwrap();

        Arc::new(Mutex::new(ret))
    };

    let destinations_orig = {
        let mut db = db.lock().unwrap();
        let tx = db.transaction().unwrap();

        let destinations = get_destinations_tx(&tx).unwrap();
        tx.commit().unwrap();

        Arc::new(RwLock::new(destinations))
    };

    let mut max_pingid: i64 = db
        .lock()
        .unwrap()
        .query_row("SELECT max(pingid) FROM pings", [], |row| {
            row.get::<_, Option<_>>(0)
        })
        .expect("could not init")
        .unwrap_or(0)
        + 1;

    let (mut tx, mut rx) =
        transport_channel(16000, Layer4(Ipv4(Icmp))).expect("could not make transport_channel");

    let mut recv = icmp_packet_iter(&mut rx);

    let (db_tx_orig, db_rx) = mpsc::channel::<DbMessage>();

    let identifier = rand::random();

    let varlink_service = {
        let db_ro =
            Connection::open_with_flags(DATABASE_PATH, OpenFlags::SQLITE_OPEN_READ_ONLY).unwrap();
        db_ro
            .create_aggregate_function(
                "wt_percentile",
                2,
                FunctionFlags::SQLITE_INNOCUOUS,
                PercentileAgg {},
            )
            .unwrap();

        let wpinger = VarlinkServer {
            db_rw: db.clone(),
            db_ro: Arc::new(Mutex::new(db_ro)),
            orig_destinations: destinations_orig.clone(),
        };

        let interface = com_westtel_wpinger::new(Box::new(wpinger));

        varlink::VarlinkService::new(
            "com.westtel.wpinger",
            "wpinger",
            env!("CARGO_PKG_VERSION"),
            "https://westtel.com",
            vec![Box::new(interface)],
        )
    };

    scope(|s| {
        let destinations = &destinations_orig;

        let db_tx = db_tx_orig.clone();
        s.spawn(move |_| {
            loop {
                match recv.next() {
                    Ok((packet, _)) => {
                        if packet.get_icmp_type() == IcmpTypes::EchoReply && EchoReplyPacket::new(packet.packet()).map(|p| p.get_identifier()) == Some(identifier) {
                            let data = &packet.packet()[EchoReplyPacket::minimum_packet_size()..];
                            let ping: PingData = match serde_json::from_slice(data) {
                                Ok(v) => v,
                                Err(_) => continue, // we want to ignore errors in parsing, aka someone else does `ping 8.8.8.8`
                            };
                            db_tx.send(DbMessage::Reply(ping.pingid, SystemTime::now())).unwrap();
                        }
                    },
                    Err(e) => println!("err: {:?}", e),
                }
            }
        });

        let db_tx = db_tx_orig.clone();
        s.spawn(move |_| {
            let mut last_printed_error: Option<Instant> = None;
            loop {
                {
                    let destinations = destinations.read().unwrap();
                    for dest in &*destinations {
                        let data = serde_json::to_string(&PingData{pingid: max_pingid}).unwrap();

                        let mut packet = vec![0; EchoRequestPacket::minimum_packet_size() + data.as_bytes().len()];

                        let mut icmp_p = MutableEchoRequestPacket::new(&mut packet).unwrap();
                        icmp_p.set_icmp_type(IcmpTypes::EchoRequest);
                        icmp_p.set_icmp_code(IcmpCode::new(0));
                        icmp_p.set_payload(data.as_bytes());
                        icmp_p.set_identifier(identifier);
                        icmp_p.set_sequence_number(max_pingid as u16);
                        let c = checksum(&IcmpPacket::new(icmp_p.packet()).unwrap());
                        icmp_p.set_checksum(c);

                        db_tx.send(DbMessage::Initial(max_pingid, SystemTime::now(), *dest)).unwrap();
                        match tx.send_to(icmp_p.to_immutable(), IpAddr::V4(*dest)) {
                            Ok(_) => {},
                            Err(e) => {
                                // only print a transmission error once every 4 minutes to prevent overloadings
                                // the logs. Errors should not normally happen (other than maybe "network not available"
                                // on startup)
                                if match last_printed_error { None => true, Some(t) => t.elapsed().as_secs_f64() > 240.0 } {
                                    // <4> to tell systemd-journald that this is a warning
                                    eprintln!("<4>unexpected error in sending to {}: {}", *dest, e);
                                    last_printed_error = Some(Instant::now());
                                }
                            },
                        };
                        max_pingid += 1;
                    }
                }
                sleep(Duration::from_secs(1));
            }
        });

        let db_tx = db_tx_orig;
        s.spawn(move |_| {
            loop {
                db_tx.send(DbMessage::Clean).unwrap();
                sleep(Duration::from_secs(60));
            }
        });
        s.spawn(move |_| {
            loop {
                let dbm = db_rx.recv().unwrap();

                match dbm {
                    DbMessage::Initial(pingid, time, ip) => {
                        db.lock().unwrap().execute("INSERT INTO pings(pingid, destination, utc) VALUES(?, ?, ?)", params!(pingid, ip.to_string(), time.duration_since(UNIX_EPOCH).unwrap().as_secs_f64())).unwrap();
                    },
                    DbMessage::Reply(pingid, time) => {
                        db.lock().unwrap().execute("UPDATE pings SET num_replys = num_replys + 1, first_reply_time = min(COALESCE(first_reply_time, 999e999), ?), last_reply_time = max(COALESCE(last_reply_time, -999e999), ?) WHERE pingid = ?",
                            params!(time.duration_since(UNIX_EPOCH).unwrap().as_secs_f64(), time.duration_since(UNIX_EPOCH).unwrap().as_secs_f64(), pingid)).unwrap();
                    },
                    DbMessage::Clean => {
                        // delete everything older than 1_209_600 seconds ago (2 weeks in seconds)
                        db.lock().unwrap().execute("DELETE FROM pings WHERE utc < ?", params!((SystemTime::now() - Duration::from_secs(1_209_600)).duration_since(UNIX_EPOCH).unwrap().as_secs_f64())).unwrap();
                    }
                }
            }
        });

        s.spawn(move |_| {
            varlink::listen(varlink_service, "unix:/run/com.westtel.wpinger", &varlink::ListenConfig::default()).unwrap();
        });
    }).unwrap();
}

fn install(
    systemd_not_booted: bool,
    wait_for_chronyc_waitsync: bool,
    systemd_at_least_241: bool,
    systemd_in_usr: bool,
) {
    if !Uid::current().is_root() {
        eprintln!("This command must be ran as root, did you forget `sudo`");
        return;
    }

    let service_file = format!(
        r#"[Unit]
Description=WestTel Pinger Service
After=time-sync.target wpinger.socket
Requires=wpinger.socket

[Service]
Type=simple
ExecStart=/usr/local/bin/wpinger run-server
{}

{}

MemoryMax=100M
# older version of MemoryMax setting
MemoryLimit=100M
TasksMax=100
IPAccounting=true
CPUAccounting=true

RestartSec=2min
Restart=always

[Install]
WantedBy=multi-user.target
"#,
        if wait_for_chronyc_waitsync {
            "# wait for chronyc waitsync\nExecStartPre=/bin/chronyc waitsync\nTimeoutStartSec=infinity"
        } else {
            ""
        },
        if systemd_at_least_241 {
            r#"# systemd at least 241
StateDirectory=wpinger
StateDirectoryMode=700
DynamicUser=yes
AmbientCapabilities=CAP_NET_RAW
"#
        } else {
            ""
        }
    );

    let socket_file = r#"[Unit]
Description=WestTel Pinger Service (Varlink)
PartOf=wpinger.service

[Socket]
ListenStream=/run/com.westtel.wpinger
SocketUser=root
SocketGroup=adm
SocketMode=0660
FileDescriptorName=varlink

RemoveOnStop=yes

[Install]
WantedBy=sockets.target
"#;

    println!("installing /usr/local/bin/wpinger...");
    let current_exe = std::env::current_exe().unwrap();
    fs::copy(current_exe, "/usr/local/bin/wpinger.tmp").unwrap();
    fs::rename("/usr/local/bin/wpinger.tmp", "/usr/local/bin/wpinger").unwrap();

    if !systemd_at_least_241 {
        println!("creating /var/lib/wpinger....");
        fs::create_dir_all("/var/lib/wpinger").unwrap();
        fs::set_permissions("/var/lib/wpinger", Permissions::from_mode(0o700)).unwrap();
    }

    println!("creating wpinger.service...");
    fs::write(
        if systemd_in_usr {
            "/usr/lib/systemd/system/wpinger.service"
        } else {
            "/etc/systemd/system/wpinger.service"
        },
        service_file,
    )
    .unwrap();

    println!("creating wpinger.socket...");
    fs::write(
        if systemd_in_usr {
            "/usr/lib/systemd/system/wpinger.socket"
        } else {
            "/etc/systemd/system/wpinger.socket"
        },
        socket_file,
    )
    .unwrap();

    if !systemd_not_booted {
        cmd!("systemctl", "daemon-reload").run().unwrap();
    }

    // start the service
    println!("starting and enabling wpinger.service...");
    cmd!("systemctl", "enable", "wpinger.service")
        .run()
        .unwrap();
    if !systemd_not_booted {
        cmd!("systemctl", "restart", "wpinger.service")
            .run()
            .unwrap();
    }

    println!("Installed!");
}
